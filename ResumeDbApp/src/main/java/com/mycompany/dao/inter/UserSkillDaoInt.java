package com.mycompany.dao.inter;

import com.mycompany.entity.UserSkill;

import java.util.List;

/**
 *
 * @author Mustafa
 */
public interface UserSkillDaoInt {

    public List<UserSkill> getAllSkillByUserId(int userId);

    public boolean insertUserSkill(UserSkill u);

    public boolean updateUserSkill(UserSkill u);

    public boolean removeUserSkill(int id);

}
