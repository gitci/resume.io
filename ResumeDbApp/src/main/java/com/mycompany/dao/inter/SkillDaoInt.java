package com.mycompany.dao.inter;

import com.mycompany.entity.Skill;

import java.util.List;

/**
 *
 * @author Mustafa
 */
public interface SkillDaoInt {
    public List<Skill> getAll();
    public Skill getById(int id);
    public List<Skill> getByName(String name);
    public boolean insertSkill(Skill skl);
    boolean updateSkill(Skill u);
    boolean removeSkill(int id);

    
}
