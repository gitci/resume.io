package com.mycompany.dao.inter;

import com.mycompany.entity.Country;


import java.util.List;

/**
 *
 * @author Mustafa
 */
public interface CountryDaoInt {

    public List<Country> getAll();

    public Country getById(int id);

    boolean updateCountry(Country u);

    boolean removeCountry(int id);

    boolean insertCountry(Country u);

}
