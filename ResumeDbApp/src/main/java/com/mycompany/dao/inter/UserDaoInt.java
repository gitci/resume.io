package com.mycompany.dao.inter;

import com.mycompany.entity.User;
import com.mycompany.entity.UserSkill;

import java.util.List;

/**
 *
 * @author Mustafa
 */
public interface UserDaoInt {
    public List<User> getAll();
    public User getByID(int id);
    public boolean updateUser(User u);
    public boolean removeUser(int id);
    public boolean addUser(User u);


    
    
    
    
    
    
}
