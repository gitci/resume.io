package com.mycompany.dao.Impl;

import com.mycompany.entity.Country;
import com.mycompany.entity.Skill;
import com.mycompany.entity.User;
import com.mycompany.entity.UserSkill;
import com.mycompany.dao.inter.AbstractDAO;
import com.mycompany.dao.inter.UserDaoInt;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mustafa
 */
public class UserDaoImpl extends AbstractDAO implements UserDaoInt {

    private User getUser(ResultSet rs) throws Exception {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        String phone = rs.getString("Phone");
        String email = rs.getString("email");
        String profileDesc = rs.getString("profile_Description");
        Date birthdate = rs.getDate("birthdate");
        String address = rs.getString("address");
        int nationalityId = rs.getInt("nationality_id");
        int birthplaceId = rs.getInt("birthplace_id");
        String nationalityStr = rs.getString("country");
        String birthplaceStr = rs.getString("birthplace");

        Country nationality = new Country(nationalityId, null, nationalityStr);
        Country birthplace = new Country(birthplaceId, birthplaceStr, null);
        return new User(id, name, surname, phone, email, profileDesc, birthdate, address, nationality, birthplace);
    }

    @Override
    public List<User> getAll() {
        List<User> result = new ArrayList<>();
        try ( Connection c = connect()) {

            Statement stm = c.createStatement();
            //stm.execute("select u.*, n.name, c.nationality as birthplace from user u left join country n on u.nationality_id=n.id left join country c on u.birth_place_id = c.id");
            stm.execute("select u.*, n.name as country,  c.nationality as birthplace from user u left join country n on u.nationality_id=n.id left join country c on u.birthplace_id = c.id");
            ResultSet rs = stm.getResultSet();

            while (rs.next()) {
                User u = getUser(rs);
                result.add(u);

            }

            c.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(result);
        return result;
    }

    @Override
    public boolean updateUser(User u) {
        try ( Connection c = connect()) {

            PreparedStatement stm = c.prepareStatement("update user set name=?, surname=?,phone=?,email=? ,profile_description=?,birthdate=?,birthplace_id=? where id=?");
            stm.setString(1, u.getName());
            stm.setString(2, u.getSurname());
            stm.setString(3, u.getPhone());
            stm.setString(4, u.getEmail());
            stm.setString(5, u.getProfileDesc());
            stm.setDate(6, u.getBirthdate());
            stm.setInt(7, u.getBirthplace().getId());
            stm.setInt(8, u.getId());

            return stm.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean removeUser(int id) {
        try ( Connection c = connect()) {

            Statement stm = c.createStatement();
            stm.execute("delete from user  where id = " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;

    }

    @Override
    public User getByID(int userID) {
        User result = null;
        try ( Connection c = connect()) {

            Statement stm = c.createStatement();
            stm.execute("select u.*, n.name as country,  c.nationality as birthplace FROM user u LEFT JOIN country n ON u.nationality_id = n.id LEFT JOIN country c ON u.birthplace_id = c.id WHERE     u.id = " + userID);

            ResultSet rs = stm.getResultSet();

            while (rs.next()) {
                result = getUser(rs);
            }

            c.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean addUser(User u) {
        try ( Connection c = connect()) {
            PreparedStatement stm = c.prepareStatement("insert into user(name,surname,phone,email,profile_description)values(?,?,?,?,?)");

            stm.setString(1, u.getName());
            stm.setString(2, u.getSurname());
            stm.setString(3, u.getPhone());
            stm.setString(4, u.getEmail());
            stm.setString(5, u.getProfileDesc());

            return stm.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

}
