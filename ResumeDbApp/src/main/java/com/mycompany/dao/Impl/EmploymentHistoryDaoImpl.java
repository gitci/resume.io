package com.mycompany.dao.Impl;

import com.mycompany.dao.inter.AbstractDAO;
import com.mycompany.dao.inter.EmploymentHistoryDaoInt;
import com.mycompany.dao.inter.UserSkillDaoInt;
import com.mycompany.entity.EmploymentsHistory;
import com.mycompany.entity.Skill;
import com.mycompany.entity.User;
import com.mycompany.entity.UserSkill;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Mustafa
 */
public class EmploymentHistoryDaoImpl extends AbstractDAO implements EmploymentHistoryDaoInt {


    private EmploymentsHistory getEmploymentHistory(ResultSet rs) throws Exception {
        String header = rs.getString("header");
        String jobDescription = rs.getString("job_description");
        Date begin_date = rs.getDate("begin_date");
        Date end_date = rs.getDate("end_date");
        int UserId = rs.getInt("user_id");
        EmploymentsHistory emp = new EmploymentsHistory(null, header, begin_date, end_date, jobDescription, new User(UserId));
        return emp;
    }

    @Override
    public List<EmploymentsHistory> getAllEmploymentHistoryByUserId(int userId) {
        List<EmploymentsHistory> result = new ArrayList<>();
        try (Connection c = connect()) {

            PreparedStatement stm = c.prepareStatement("select * from employment_history where user_id= ?");
            stm.setInt(1, userId);
            stm.execute();
            ResultSet rs = stm.getResultSet();

            while (rs.next()) {
                EmploymentsHistory employmentHistory = getEmploymentHistory(rs);
                result.add(employmentHistory);

            }

            c.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

}
