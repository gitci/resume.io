package com.mycompany.dao.Impl;

import com.mycompany.dao.inter.AbstractDAO;
import com.mycompany.dao.inter.UserDaoInt;
import com.mycompany.dao.inter.UserSkillDaoInt;
import com.mycompany.entity.Country;
import com.mycompany.entity.Skill;
import com.mycompany.entity.User;
import com.mycompany.entity.UserSkill;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mustafa
 */
public class UserSkillDaoImpl extends AbstractDAO implements UserSkillDaoInt {


    private UserSkill getUserSkill(ResultSet rs) throws Exception {
        int userSkillId = rs.getInt("userSkillId");
        int userid = rs.getInt("id");
        int skillID = rs.getInt("skill_id");
        //int userID = rs.getInt("user_id");
        String skillName = rs.getString("skill_name");
        int power = rs.getInt("power");
        UserSkill us = new UserSkill(userSkillId, new User(userid), new Skill(skillID, skillName), power);
        return us;

    }

    @Override
    public List<UserSkill> getAllSkillByUserId(int userId) {
        List<UserSkill> result = new ArrayList<>();
        try (Connection c = connect()) {

            PreparedStatement stm = c.prepareStatement("SELECT   us.id as UserSkillid, u.*, us.skill_id, us.power, s.name as skill_name  FROM  user_skill us    LEFT JOIN     user u ON us.user_id = u.id LEFT JOIN skill s ON us.skill_id = s.id     where us.user_id=?");
            stm.setInt(1, userId);
            stm.execute();
            ResultSet rs = stm.getResultSet();

            while (rs.next()) {
                UserSkill u = getUserSkill(rs);
                result.add(u);

            }

            c.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

     public boolean insertUserSkill(UserSkill u) {
        Connection conn;
        boolean b = true;
        try {
            conn = connect();
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO user_skill (skill_id , user_id ,power) VALUES (? , ? ,  ? ) ");

            stmt.setInt(1, u.getSkill().getId());
            stmt.setInt(2, u.getUser().getId());
            stmt.setInt(3, u.getPower());

            b = stmt.execute();

        } catch (Exception ex) {
            System.err.println(ex);
            b = false;
        }
        return b;
    }

    public boolean updateUserSkill(UserSkill u) {
        Connection conn;
        boolean b = true;
        try {
            conn = connect();
            PreparedStatement stmt = conn.prepareStatement("UPDATE user_skill SET skill_id = ? , user_id =? ,power =?  WHERE id = ? ");

            stmt.setInt(1, u.getSkill().getId());
            stmt.setInt(2, u.getUser().getId());
            stmt.setInt(3, u.getPower());

            stmt.setInt(4, u.getId());

            b = stmt.execute();

        } catch (Exception ex) {
            System.err.println(ex);
            b = false;
        }
        return b;
    }

    @Override
    public boolean removeUserSkill(int id) {
        Connection conn;
        try {
            conn = connect();

            PreparedStatement stmt = conn.prepareStatement("DELETE FROM user_skill  WHERE ID=?;");
            stmt.setInt(1, id);
            System.out.println("id :" + String.valueOf(id));
            return stmt.execute();

        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }
}
