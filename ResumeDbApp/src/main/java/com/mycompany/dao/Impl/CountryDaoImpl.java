package com.mycompany.dao.Impl;

import com.mycompany.dao.inter.AbstractDAO;
import com.mycompany.dao.inter.CountryDaoInt;
import com.mycompany.entity.Country;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;

public class CountryDaoImpl extends AbstractDAO implements CountryDaoInt {

    public Country getCountry(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String nationality = rs.getString("nationality");

        Country country = new Country(id, name, nationality);
        return country;

    }

    @Override
    public List<Country> getAll() {
        List<Country> list = new ArrayList<>();
        Connection con;
        String sql = ("SELECT * FROM country");
        try {
            con = connect();
            Statement stmt = con.createStatement();

            stmt.execute(sql);
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                Country country = getCountry(rs);
                list.add(country);
            }
        } catch (Exception e) {
        }
        return list;
    }

    @Override
    public Country getById(int id) {
        Country country =null;
       
        Connection con;
        try {
            con = connect();
            PreparedStatement stmt = con.prepareStatement("Select * from country where id = ?");
            stmt.setInt(1, id);
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                country = getCountry(rs);
            }

        } catch (Exception e) {
        }

        return country;
    }

    @Override
    public boolean updateCountry(Country u) {
        Connection conn;
        boolean b = true;
        try {
            conn = connect();
            PreparedStatement stmt = conn.prepareStatement("UPDATE country SET name=?,nationality=? WHERE id= ?");
            stmt.setString(1, u.getName());
            stmt.setString(2, u.getNationality());
            stmt.setInt(3, u.getId());
            b = stmt.execute();

        } catch (Exception ex) {
            System.err.println(ex);
            b = false;
        }
        return b;
    }

    @Override
    public boolean removeCountry(int id) {
       Connection conn;
        try {
            conn = connect();

            PreparedStatement stmt = conn.prepareStatement("DELETE FROM country WHERE id=?;");
            stmt.setInt(1, id);

            return stmt.execute();
        } catch (Exception ex) {
            return false;
        }
    }
    

    @Override
    public boolean insertCountry(Country u) {
       Connection conn;
        boolean b = true;
        try {
            conn = connect();
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO country (name ,nationality) VALUES (?,?)",Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, u.getName());
            stmt.setString(2, u.getNationality());
            b = stmt.execute();
            ResultSet genKeys = stmt.getGeneratedKeys();
            if(genKeys.next()){
                u.setId(genKeys.getInt(1));
            }
        } catch (Exception ex) {
            System.err.println(ex);
            b = false;
        }
        return b;
    }

}
