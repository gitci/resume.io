package com.mycompany.Main;

import com.mycompany.dao.Impl.CountryDaoImpl;
import com.mycompany.dao.Impl.EmploymentHistoryDaoImpl;
import com.mycompany.dao.Impl.SkillDaoImpl;
import com.mycompany.dao.Impl.UserDaoImpl;
import com.mycompany.dao.Impl.UserSkillDaoImpl;
import com.mycompany.dao.inter.CountryDaoInt;
import com.mycompany.dao.inter.EmploymentHistoryDaoInt;
import com.mycompany.dao.inter.SkillDaoInt;
import com.mycompany.dao.inter.UserDaoInt;
import com.mycompany.dao.inter.UserSkillDaoInt;

/**
 * @author Mustafa
 */
public class Context {
    
    public static UserDaoInt instaceUserDao(){
        return new UserDaoImpl();
    }

    public static UserSkillDaoInt instanceUserSkillDao(){
        return  new UserSkillDaoImpl();

    }
    public static EmploymentHistoryDaoInt instanceEmploymentHistoryDao(){
        return new EmploymentHistoryDaoImpl();
    }
    public static CountryDaoInt instanceCountryDao(){
        return new CountryDaoImpl();
    }
    public static SkillDaoInt instanceiSkillDao(){
        return new SkillDaoImpl();
    }
}
