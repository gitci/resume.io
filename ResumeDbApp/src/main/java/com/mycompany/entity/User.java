package com.mycompany.entity;

import java.sql.Date;
import java.util.List;

/**
 * @author Mustafa
 */
public class User {
    public int id;
    private String name;
    private String surname;
    private String email;
    private String profileDesc;
    private String phone;
    private Date birthdate;
    private String address;
    private Country nationaly;
    private Country birthplace;
    private List<UserSkill>  skills;

    public User(int id, String name, String surname, String phone, String email,String profileDesc,  Date birthdate, String address, Country nationaly, Country birthplace) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.birthdate = birthdate;
        this.nationaly = nationaly;
        this.birthplace = birthplace;
        this.skills = skills;
        this.profileDesc = profileDesc;
        this.address = address;
    }

    public User() {
        
    }
    public User(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileDesc() {
        return profileDesc;
    }

    public void setProfileDesc(String profileDesc) {
        this.profileDesc = profileDesc;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Country getNationaly() {
        return nationaly;
    }

    public void setNationaly(Country nationaly) {
        this.nationaly = nationaly;
    }

    public Country getBirthplace() {
        return birthplace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBirthplace(Country birthplace) {
        this.birthplace = birthplace;
    }

    public List<UserSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<UserSkill> skills) {
        this.skills = skills;
    }

//    @Override
//    public String toString() {
//        return "User{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", email=" + email + ", profileDesc=" + profileDesc + ", phone=" + phone + ", birthdate=" + birthdate + ", nationaly=" + nationaly + ", birthplace=" + birthplace + ", skills=" + skills + '}';
//    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", email=" + email + ", profileDesc=" + profileDesc + ", phone=" + phone + ", birthdate=" + birthdate + ", address=" + address + ", nationaly=" + nationaly + ", birthplace=" + birthplace + ", skills=" + skills + '}';
    }
    
    

    

   

  

   
    

}
