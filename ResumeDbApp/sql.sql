-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema resume
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema resume
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `resume` DEFAULT CHARACTER SET utf8 ;
USE `resume` ;

-- -----------------------------------------------------
-- Table `resume`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resume`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `nationality` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `resume`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resume`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(35) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(90) NOT NULL,
  `phone` VARCHAR(17) NOT NULL,
  `profileDescription` TEXT NULL DEFAULT NULL,
  `address` VARCHAR(200) NULL DEFAULT NULL,
  `birthdate` DATE NULL DEFAULT NULL,
  `birthplace_id` INT NULL DEFAULT NULL,
  `nationality_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `n_idfk_idx` (`nationality_id` ASC) VISIBLE,
  INDEX `birth_placeFK1_idx` (`birthplace_id` ASC) VISIBLE,
  CONSTRAINT `birth_placeFK1`
    FOREIGN KEY (`birthplace_id`)
    REFERENCES `resume`.`country` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `n_idfk`
    FOREIGN KEY (`nationality_id`)
    REFERENCES `resume`.`country` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `resume`.`employment_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resume`.`employment_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `header` VARCHAR(255) NOT NULL,
  `begin_date` DATE NOT NULL,
  `end_date` DATE NULL DEFAULT NULL,
  `jobdescription` VARCHAR(255) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `emp_userfk_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `emp_userfk`
    FOREIGN KEY (`user_id`)
    REFERENCES `resume`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `resume`.`skill`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resume`.`skill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `resume`.`user_skill`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resume`.`user_skill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  `power` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `skill_userfk_idx` (`user_id` ASC) VISIBLE,
  INDEX `skillidFK_idx` (`skill_id` ASC) VISIBLE,
  CONSTRAINT `skill_userfk`
    FOREIGN KEY (`user_id`)
    REFERENCES `resume`.`user` (`id`),
  CONSTRAINT `skillidFK`
    FOREIGN KEY (`skill_id`)
    REFERENCES `resume`.`skill` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
