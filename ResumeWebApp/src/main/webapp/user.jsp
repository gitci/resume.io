<%-- 
    Document   : user
    Created on : Mar 23, 2023, 2:30:49 PM
    Author     : Mustafa
--%>

<%@page import="com.mycompany.dao.inter.UserDaoInt"%>
<%@page import="com.mycompany.Main.Context"%>
<%@page import="com.mycompany.entity.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            UserDaoInt userDao = Context.instaceUserDao();
            User u = userDao.getByID(7);
        %>
        <div>
            <form action="UserController" method="POST">
                <input type="hidden" name="id" value="<%=u.getId()%>"
                       <label for="name">name:</label>
                <input type ="text" name="name" value="<%=u.getName()%>"/>
                <br/>
                <label for="surname">Surname:</label>
                <input type ="text" name="surname" value="<%=u.getSurname()%>"/>
                <br/>
                <input type="submit" name="save" value="save"/>
            </form>
        </div>
    </body>
</html>
