package com.mycompany.resumewebapp;

import com.mycompany.Main.Context;
import com.mycompany.dao.inter.UserDaoInt;
import com.mycompany.entity.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mustafa
 */
@WebServlet(name = "UserController", urlPatterns = {"/UserController"})
public class UserController extends HttpServlet {

     private UserDaoInt userDao= Context.instaceUserDao();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            int id = Integer.valueOf(request.getParameter("id"));
            String name = request.getParameter("name");
            String surname =request.getParameter("surname");
            System.out.println("name = "+ name);
            System.out.println("Surname = "+surname);
            User user = userDao.getByID(id);
            user.setName(name);
            user.setSurname(surname);
            userDao.updateUser(user);
            response.sendRedirect("user.jsp");
            
    }

  

}
