package com.mycompany.resumewebapp;

import com.mycompany.dao.Impl.SkillDaoImpl;
import com.mycompany.dao.Impl.UserDaoImpl;
import com.mycompany.dao.inter.SkillDaoInt;
import com.mycompany.dao.inter.UserDaoInt;
import com.mycompany.entity.Skill;
import com.mycompany.entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mustafa
 */
@WebServlet(name = "MyFavoritePage", urlPatterns = {"/MyFavoritePage"})
public class MyFavoritePage extends HttpServlet {

    private SkillDaoInt skillDao = new SkillDaoImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try ( PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyFavoritePage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(skillDao.getAll() + "<br>");

            out.println("</body>");
            out.println("</html>");
        }

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String name = String.valueOf(req.getParameter("name"));
        Skill s = new Skill(0, name);
        boolean b = skillDao.insertSkill(s);

        try ( PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyFavoritePage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>I GOT POST REQUEST </h1>");
            out.println("skill inserted" + s + "<br>");
            if (b) {
                out.println("<h1>Successfly INSERTED</h1>");
            } else {
                out.println("<h1>NOT INSERT</h1>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

}
